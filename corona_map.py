# -*- coding: utf-8 -*-
"""
Created on Sat Aug 01 17:51:23 2020

@author: Harley
"""
import branca
import csv
import folium
import json
import pandas as pd

# data from - https://covidlive.com.au/vic/postcode
csv_path = '31-July-2020.csv'
df = pd.read_csv(csv_path)

# sum the number of incidents recorded for each suburb
df = df.groupby(['postcode'])['active'].agg(
    # make the numbers numeric otherwise it just concatenates strings
    lambda x: pd.to_numeric(x, errors='coerce').sum()
)

# print(df.to_string())

# create a dictionary, where keys are postcode and values are number of active COVID-19 cases
postcode_dict = df.to_dict()
# keys must be Strings, values int
postcode_dict = {str(k): int(v) for k, v in postcode_dict.items()}
print(postcode_dict)


# with open('new.geojson', 'r') as f:
with open('victorian-postal-areas-2016.geojson', 'r') as f:
    geodata = json.load(f)

# A Python dictionary containing properties to be added to each GeoJSON Feature
# postcode_dict

# Loop over GeoJSON features and add the new properties
# newdata = []
for postcode_area in geodata['features']:
    # newdata.append(postcode_area)
    for k, v in postcode_area['properties'].items(): # geodata['features'][0]['properties'].items():
        # print(k, v) # in postcode_dict:
        if k == 'POA_CODE16':
            # postcode_area['ACTIVE'] = postcode_dict[v]
            # print(postcode_dict[v])
            # postcode_area['properties']['ACTIVE'] = postcode_dict[v]
            # new_list_item = postcode_area
            # new_list_item['properties']['ACTIVE'] = postcode_dict[v]
            # newdata.append(new_list_item)
            if v in postcode_dict:
                # newdata[-1]['properties']['POA_NAME16'] = postcode_dict[v]
                postcode_area['properties']['POA_NAME16'] = postcode_dict[v]
            else:
                postcode_area['properties']['POA_NAME16'] = '0'
            postcode_area['properties']['ACTIVE'] = postcode_area['properties'].pop('POA_NAME16')
            # pass
            # print(type(newdata))
            # print(type(newdata[-1]['properties']))

# Write result to a new file
with open('new.geojson', 'w') as f:
    json.dump(geodata, f)


colormap = branca.colormap.linear.YlOrRd_09.scale(0, 350)
colormap = colormap.to_step(index=[0, 50, 100, 150, 200, 250, 300, 350])
colormap.caption = 'Active Cases 31-July-2020'


def style_function(feature):
    # POA_CODE16
    suburb = postcode_dict.get(feature['properties']['POA_CODE16'])
    return {
        'fillColor': '#gray' if suburb is None else colormap(suburb),
        'fillOpacity': 0.6,
        # borders
        'weight': 0.2,
    }


world_map = folium.Map(
    location=[-38.292102, 144.727880],
    zoom_start=8,
    tiles='openstreetmap'
)

folium.GeoJson(
    data='new.geojson',
    style_function=style_function,
    tooltip=folium.features.GeoJsonTooltip(fields=['POA_CODE16', 'AREASQKM16', 'ACTIVE'],
                                           aliases=['Postcode', 'Area sq. km', 'Active Cases']
                                           )
).add_to(world_map)

colormap.add_to(world_map)
world_map.save('31_July_2020.html')
